import { Building, createBuilding } from "../../src/domain/building/entities/building";
import { CreateBuildingRequestBody } from "../../src/presentation/rest/controllers/building.controller";
import { generateRandomObjectId } from './../../src/common/random-object-id';

export function generateBuilding(partialBuilding: Partial<Building> = {}): Building {
  return {
    ...createBuilding({
      number: "500000000",
      name: "Tour Eiffel",
      type: "BuildingBase",
      agencyId: generateRandomObjectId(),
      origin: null,
      address: {
        address1: null,
        zipCode: null,
        city: null,
        completeAddress: "",
      }
    }),
    ...partialBuilding,
  };
}

export async function insertBuilding(body: CreateBuildingRequestBody): Promise<Building> {
  const response = await fetch("http://localhost:3999/building", {
    headers: {"content-type": "application/json"},
    method: "POST",
    body: JSON.stringify(body)
  });
  return response.json();
}

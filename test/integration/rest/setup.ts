import { NestExpressApplication } from "@nestjs/platform-express";

import { initializeApplication } from "../../../src/app";

let app: NestExpressApplication;

export async function setup (): Promise<void> {
  app = await initializeApplication();
  await app.listen(3999);
}

export async function teardown (): Promise<void> {
  await app.close();
}

import fetch, { Response } from 'node-fetch';
import { beforeEach, describe, expect, it } from "vitest";
import { CreateBuildingRequestBody } from "../../../../src/presentation/rest/controllers/building.controller";
import { generateRandomObjectId } from './../../../../src/common/random-object-id';
import { CreateBuildingResponseBody } from './../../../../src/presentation/rest/controllers/building.controller';

describe('CreateBuilding', () => {
  let promise: Promise<Response>;
  const name = "Hello";
  const type = "BuildingComplet";
  const agencyId = generateRandomObjectId();

  beforeEach(() => {
    setup({
      name,
      type,
      agencyId,
    });
  });
  it("should create building", async () => {
    const result = await promise;

    expect(await result.json()).toEqual(<CreateBuildingResponseBody>{
      id: expect.any(String),
      name,
      type,
      agencyId,
    });
  });

  function setup(body: CreateBuildingRequestBody): void {
    promise = fetch("http://localhost:3999/building", {
      headers: {"content-type": "application/json"},
      method: "POST",
      body: JSON.stringify(body)
    });
  }
});

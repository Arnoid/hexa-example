import fetch, { Response } from 'node-fetch';
import { beforeEach, describe, expect, it } from "vitest";
import { Building } from '../../../../src/domain/building/entities/building';
import { UpdateBuildingRequestBody, UpdateBuildingResponseBody } from "../../../../src/presentation/rest/controllers/building.controller";
import { generateBuilding, insertBuilding } from '../../../utils/building.utils';

describe('UpdateBuilding', () => {
  let promise: Promise<Response>;

  const building = generateBuilding();

  beforeEach(async () => {
    await setup(building.id, { name: "Tour Montparnasse", type: "BuildingBase" }, [building]);
  });
  it("should create building", async () => {
    const result = await promise;

    expect(await result.json()).toEqual(<UpdateBuildingResponseBody>{
      ...building,
      name: "Tour Montparnasse",
      type: "BuildingBase",
    });
  });

  async function setup(id: string, body: UpdateBuildingRequestBody, buildings: Building[] = []): Promise<void> {
    await Promise.all(buildings.map(async (building) => {
      await insertBuilding({ name: building.name, type: building.type, agencyId: building.agencyId });
    }));
    promise = fetch(`http://localhost:3999/building/${id}`, {
      headers: {"content-type": "application/json"},
      method: "PUT",
      body: JSON.stringify(body)
    });
  }
});

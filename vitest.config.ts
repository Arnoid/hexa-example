import swc from "unplugin-swc";
import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    environment: "node",
    globalSetup: ["./test/integration/rest/setup.ts"],
    globals: true,
  },
  plugins: [swc.vite()],
});

import swc from "unplugin-swc";
import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    environment: "node",
    include: ['src/**/*.spec.ts'],
  },
  plugins: [swc.vite()],
});

import { NestFactory } from "@nestjs/core";
import { ExpressAdapter, NestExpressApplication } from "@nestjs/platform-express";
import { json } from "body-parser";
import express from "express";
import helmet from "helmet";

import { AppModule } from "./app.module";
import { Mongodb } from "./infrastructure/mongodb";

export async function initializeApplication(): Promise<NestExpressApplication> {
  const adapter = new ExpressAdapter(express());
  adapter.use(helmet());
  adapter.enableCors({
    credentials: true,
    preflightContinue: true,
  });
  adapter.use(json({ limit: "2mb" }));
  const app = await NestFactory.create<NestExpressApplication>(AppModule, adapter, {
    bodyParser: true,
  });

  await new Mongodb().connect();

  return app;
}

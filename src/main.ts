import { initializeApplication } from "./app";

async function startApplication(): Promise<void> {
  const app = await initializeApplication();
  await app.listen(3999);
}

export default startApplication();

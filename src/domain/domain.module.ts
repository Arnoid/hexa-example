import { Module } from "@nestjs/common";
import { BuildingModule } from "./building/building.module";

const domainModules = [
  BuildingModule,
];

@Module({
  exports: domainModules,
  imports: domainModules,
})
export class DomainModule {}
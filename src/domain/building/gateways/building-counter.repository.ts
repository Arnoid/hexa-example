export interface BuildingCounterRepository {
  getNextNumber(): Promise<string>;
}
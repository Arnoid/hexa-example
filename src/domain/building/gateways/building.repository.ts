import { Building } from "../entities/building";

export interface BuildingRepository {
  create(building: Building): Promise<void>;
  findOneById(id: string): Promise<Building | null>;
  update(building: Building): Promise<void>;
  deleteById(id: string): Promise<void>;
}
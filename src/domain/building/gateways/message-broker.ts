import { BuildingEvent } from "../value-objects/event";

export interface MessageBroker {
  send(key: BuildingEvent, message: any): Promise<void>;
}
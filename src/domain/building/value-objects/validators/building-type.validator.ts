import { z } from "zod";

export const buildingTypeSchema = z.enum([
  "BuildingComplet",
  "BuildingBase",
])
  import { z } from "zod";

  export const buildingTypeSchema = z.enum([
    "MISSION_OLD_BUILDING_CREATION",
    "MISSION_NEW_MANDATE",
    "MISSION_RCP_UPDATE",
  ]);
  
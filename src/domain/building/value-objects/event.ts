export type BuildingEvent = "building.created" | "building.updated";

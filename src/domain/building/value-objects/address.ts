
export const getCompleteAddress = (address: {
  address1: string | null,
  zipCode: string | null,
  city: string | null,
}): string => {
  return [address.address1, address.zipCode, address.city]
    .filter(Boolean)
    .join(" ");
};
export type BuildingCounter = {
  key: string;
  value: number;
}
import { generateRandomObjectId } from "../../../common/random-object-id";
import { WithRequired } from "../../../common/with-required";
import { getCompleteAddress } from "../value-objects/address";
import { BuildingType } from "../value-objects/building-type";
import { MissionKind } from "../value-objects/mission-kind";

export type Origin = {
  target: string;
  kind: MissionKind;
};

export type Building = {
  id: string;
  name: string;
  number: string;
  type: BuildingType;
  version: number;
  agencyId: string;
  isBeingDevelopedBy: Origin[] | null;
  origin: Origin | null;
  address: {
    address1: string | null;
    zipCode: string | null;
    city: string | null;
    completeAddress: string;
  };
};

export function createBuilding(partialBuilding: Omit<
  WithRequired<
    Partial<Building>,
    "name" |
    "type" |
    "number" |
    "agencyId" |
    "origin" |
    "address"
  >, "id" | "version" | "isBeingDevelopedBy">): Building {
  return {
    ...partialBuilding,
    id: generateRandomObjectId(),
    version: 1,
    isBeingDevelopedBy: partialBuilding.origin ? [partialBuilding.origin] : null,
    address: {
      ...partialBuilding.address,
      completeAddress: getCompleteAddress(partialBuilding.address)
    }
  }
}

export function updateBuilding(building: Building, update: Omit<Partial<Building>, "version">): Building {
  const address = {
    ...building.address,
    ...update.address ?? {},
  };
  return {
    ...building,
    ...update,
    version: building.version + 1,
    address: {
      ...address,
      completeAddress: getCompleteAddress(address)
    }
  }
}
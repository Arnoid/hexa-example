import { Module } from "@nestjs/common";

import { InfrastructureModule } from "../../infrastructure/infrastructure.module";
import { CreateBuildingUseCase } from "./use-cases/create-building/create-building.use-case";
import { GetBuildingUseCase } from "./use-cases/get-building/get-building.use-case";
import { UpdateBuildingUseCase } from "./use-cases/update-building/update-building.use-case";

const useCases = [
  CreateBuildingUseCase,
  UpdateBuildingUseCase,
  GetBuildingUseCase,
];

@Module({
  exports: useCases,
  imports: [InfrastructureModule],
  providers: useCases,
})
export class BuildingModule {}

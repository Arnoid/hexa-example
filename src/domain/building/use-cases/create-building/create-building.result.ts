import { Building } from "../../entities/building";

export type CreateBuildingResult = Building;
import { Origin } from "../../entities/building";
import { BuildingType } from "../../value-objects/building-type";

export type CreateBuildingPort = {
  name: string;
  type: BuildingType;
  agencyId: string;
  origin: Origin | null;
  address: {
    address1: string | null;
    zipCode: string | null;
    city: string | null;
  }
};

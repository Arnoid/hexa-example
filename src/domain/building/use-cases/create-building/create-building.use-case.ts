import { Inject, Injectable } from '@nestjs/common';
import { BUILDING_COUNTER_REPOSITORY, BUILDING_REPOSITORY } from '../../../../common/injectable.constant';
import { Building, createBuilding } from '../../entities/building';
import { BuildingCounterRepository } from '../../gateways/building-counter.repository';
import { BuildingRepository } from '../../gateways/building.repository';
import { MessageBroker } from '../../gateways/message-broker';
import { MESSAGE_BROKER } from './../../../../common/injectable.constant';
import { CreateBuildingPort } from './create-building.port';
import { CreateBuildingResult } from './create-building.result';

@Injectable()
export class CreateBuildingUseCase {
  constructor(
    @Inject(BUILDING_REPOSITORY)
    private readonly buildingRepository: BuildingRepository,
    @Inject(BUILDING_COUNTER_REPOSITORY)
    private readonly buildingCounterRepository: BuildingCounterRepository,
    @Inject(MESSAGE_BROKER)
    private readonly messageBroker: MessageBroker,
  ) {}
  async execute(port: CreateBuildingPort): Promise<CreateBuildingResult> {
    const number = await this.buildingCounterRepository.getNextNumber();
    const building: Building = createBuilding({
      name: port.name,
      type: port.type,
      number,
      agencyId: port.agencyId,
      origin: port.origin,
      address: {
        ...port.address,
        completeAddress: "",
      },
    });
    await this.buildingRepository.create(building);
    await this.messageBroker.send("building.created", building);
    return Promise.resolve(building);
  }
}
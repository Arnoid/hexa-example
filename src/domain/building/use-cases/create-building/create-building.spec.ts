import { beforeEach, describe, expect, it } from "vitest";
import { generateRandomObjectId } from "../../../../common/random-object-id";
import { InMemoryMessageBroker } from "../../../../infrastructure/message-broker/in-memory.message-broker";
import { InMemoryBuildingCounterRepository } from "../../../../infrastructure/repositories/building-counter/in-memory/in-memory.building-counter.repository";
import { InMemoryBuildingRepository } from '../../../../infrastructure/repositories/building/in-memory/in-memory.building.repository';
import { Building, Origin } from "../../entities/building";
import { CreateBuildingPort } from './create-building.port';
import { CreateBuildingResult } from './create-building.result';
import { CreateBuildingUseCase } from './create-building.use-case';

describe("CreateBuildingUseCase", () => {
  let promise: Promise<CreateBuildingResult>;

  let buildingRepository: InMemoryBuildingRepository;
  let buildingCounterRepository: InMemoryBuildingCounterRepository;
  let messageBroker: InMemoryMessageBroker;

  const name = "Tour Eiffel";
  const type = "BuildingComplet";
  const number = "500000000";
  const agencyId = generateRandomObjectId();
  const origin: Origin = {
    target: generateRandomObjectId(),
    kind: "MISSION_NEW_MANDATE",
  };

  let expectedBuilding: Building;

  describe("when address is provided", () => {
    beforeEach(() => {
      expectedBuilding = {
        id: expect.any(String),
        name,
        type,
        number,
        version: 1,
        agencyId,
        origin,
        isBeingDevelopedBy: [origin],
        address: {
          address1: "76 - 78 Avenue des Champs Elysées",
          zipCode: "75008",
          city: "PARIS",
          completeAddress: "76 - 78 Avenue des Champs Elysées 75008 PARIS"
        },
      };

      setup({
        name,
        type: "BuildingComplet",
        agencyId,
        origin,
        address: {
          address1: "76 - 78 Avenue des Champs Elysées",
          zipCode: "75008",
          city: "PARIS",
        },
      }, [], [number]);
    });
    it("should return building", async () => {
      const result = await promise;

      expect(result).toEqual(expectedBuilding)
    });
    it("should have created a building", async () => {
      await promise;

      expect(buildingRepository.buildings).toEqual([expectedBuilding]);
    });
    it("should have emitted an event", async () => {
      await promise;

      expect(messageBroker.messages).toEqual([{key: "building.created",message: expectedBuilding}]);
    });
  });
  describe("when origin is provided", () => {
    beforeEach(() => {
      expectedBuilding = {
        id: expect.any(String),
        name,
        type,
        number,
        version: 1,
        agencyId,
        origin,
        isBeingDevelopedBy: [origin],
        address: {
          address1: null,
          zipCode: null,
          city: null,
          completeAddress: "",
        }
      };
      setup({
        name,
        type: "BuildingComplet",
        agencyId,
        origin,
        address: {
          address1: null,
          zipCode: null,
          city: null,
        },
      }, [], [number]);
    });
    it("should return building", async () => {
      const result = await promise;

      expect(result).toEqual(expectedBuilding)
    });
    it("should have created a building", async () => {
      await promise;

      expect(buildingRepository.buildings).toEqual([expectedBuilding]);
    });
    it("should have emitted an event", async () => {
      await promise;

      expect(messageBroker.messages).toEqual([{
        key: "building.created",
        message: expectedBuilding,
      }]);
    });
  });

  describe("when origin is not provided", () => {
    beforeEach(() => {
      expectedBuilding = {
        id: expect.any(String),
        name,
        type,
        number,
        version: 1,
        agencyId,
        origin: null,
        isBeingDevelopedBy: null,
        address: {
          address1: null,
          zipCode: null,
          city: null,
          completeAddress: "",
        }
      };
      setup({
        name,
        type: "BuildingComplet",
        agencyId,
        origin: null,
        address: {
          address1: null,
          zipCode: null,
          city: null,
        },
      }, [], [number]);
    });
    it("should return building", async () => {
      const result = await promise;

      expect(result).toEqual(expectedBuilding)
    });
    it("should have created a building", async () => {
      await promise;

      expect(buildingRepository.buildings).toEqual([expectedBuilding]);
    });
    it("should have emitted an event", async () => {
      await promise;

      expect(messageBroker.messages).toEqual([{
        key: "building.created",
        message: expectedBuilding,
      }]);
    });
  });

  function setup(port: CreateBuildingPort, buildings: Building[] = [], numbers: string[] = []) {
    buildingRepository = new InMemoryBuildingRepository(buildings);
    buildingCounterRepository = new InMemoryBuildingCounterRepository(numbers);
    messageBroker = new InMemoryMessageBroker();
    const useCase = new CreateBuildingUseCase(buildingRepository, buildingCounterRepository, messageBroker)
    promise = useCase.execute(port);
  }
});
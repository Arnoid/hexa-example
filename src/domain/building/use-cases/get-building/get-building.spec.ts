import { beforeEach, describe, expect, it } from "vitest";
import { generateBuilding } from "../../../../../test/utils/building.utils";
import { NotFoundError } from "../../../../common/errors/not-found.error";
import { generateRandomObjectId } from "../../../../common/random-object-id";
import { Building } from "../../entities/building";
import { InMemoryBuildingRepository } from './../../../../infrastructure/repositories/building/in-memory/in-memory.building.repository';
import { GetBuildingPort } from "./get-building.port";
import { GetBuildingResult } from "./get-building.result";
import { GetBuildingUseCase } from "./get-building.use-case";

describe("GetBuildingUseCase", () => {
  let promise: Promise<GetBuildingResult>;

  let buildingRepository: InMemoryBuildingRepository;

  const building = generateBuilding();

  describe("when building is found", () => {
    beforeEach(() => {
      setup({ id: building.id }, [building]);
    });

    it("should return building", async () => {
      const result = await promise;

      expect(result).toEqual(building);
    });
  });

  describe("when building is not found", () => {
    beforeEach(() => {
      setup({ id: generateRandomObjectId() });
    });

    it("should throw a not found error", () => {
      expect(promise).rejects.toThrow(NotFoundError);
    });
  });
  function setup(port: GetBuildingPort, buildings: Building[] = []): void {
    buildingRepository = new InMemoryBuildingRepository(buildings)
    const useCase = new GetBuildingUseCase(buildingRepository);
    promise = useCase.execute(port);
  }
});

import { Inject } from "@nestjs/common";
import { NotFoundError } from "../../../../common/errors/not-found.error";
import { BUILDING_REPOSITORY } from "../../../../common/injectable.constant";
import { BuildingRepository } from "../../gateways/building.repository";
import { GetBuildingPort } from "./get-building.port";
import { GetBuildingResult } from "./get-building.result";

export class GetBuildingUseCase {
  constructor(
    @Inject(BUILDING_REPOSITORY)
    private readonly buildingRepository: BuildingRepository,
  ) {}
  async execute(port: GetBuildingPort): Promise<GetBuildingResult> {
    const building = await this.buildingRepository.findOneById(port.id);

    if (!building) {
      throw new NotFoundError({ id: port.id });
    }

    return Promise.resolve(building);
  }
}
import { Building } from "../../entities/building";

export type GetBuildingResult = Building;
import { Inject } from "@nestjs/common";
import { BuildingRepository } from "../../gateways/building.repository";
import { BUILDING_REPOSITORY } from './../../../../common/injectable.constant';
import { DeleteBuildingPort } from "./delete-building.port";
import { DeleteBuildingResult } from "./delete-building.result";

export class DeleteBuildingUseCase {
  constructor(
    @Inject(BUILDING_REPOSITORY)
    private readonly buildingRepository: BuildingRepository,
  ) {}
  execute(port: DeleteBuildingPort): Promise<DeleteBuildingResult> {
    return this.buildingRepository.deleteById(port.id);
  }
}
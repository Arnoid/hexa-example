import { beforeEach, describe, expect, it } from "vitest";
import { generateBuilding } from "../../../../../test/utils/building.utils";
import { InMemoryBuildingRepository } from './../../../../infrastructure/repositories/building/in-memory/in-memory.building.repository';
import { Building } from './../../entities/building';
import { DeleteBuildingPort } from "./delete-building.port";
import { DeleteBuildingResult } from "./delete-building.result";
import { DeleteBuildingUseCase } from "./delete-building.use-case";

describe("DeleteBuildingUseCase", () => {
  let promise: Promise<DeleteBuildingResult>;

  let buildingRepository: InMemoryBuildingRepository;

  const building = generateBuilding();
  describe("when building exists", () => {
    beforeEach(() => {
      setup({ id: building.id }, [building]);
    });

    it("should return nothing", async () => {
      const result = await promise;

      expect(result).toEqual(undefined) 
    });
    it("should have deleted building", async () => {
      await promise;

      expect(buildingRepository.buildings).toEqual([]);
    });
  });

  function setup(port: DeleteBuildingPort, buildings: Building[] = []) {
    buildingRepository = new InMemoryBuildingRepository(buildings);
    const useCase = new DeleteBuildingUseCase(buildingRepository);
    promise = useCase.execute(port);
  }
});
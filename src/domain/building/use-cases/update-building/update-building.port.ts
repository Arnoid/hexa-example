import { BuildingType } from "../../value-objects/building-type";

export type UpdateBuildingPort = {
  id: string;
  name?: string;
  type?: BuildingType;
  agencyId?: string;
  address?: {
    address1: string | null;
    zipCode: string | null;
    city: string | null;
  }
};

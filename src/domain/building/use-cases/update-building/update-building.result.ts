import { Building } from "../../entities/building";

export type UpdateBuildingResult = Building;
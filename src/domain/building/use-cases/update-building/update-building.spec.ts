import { beforeEach, describe, expect, it } from 'vitest';
import { generateBuilding } from '../../../../../test/utils/building.utils';
import { NotFoundError } from '../../../../common/errors/not-found.error';
import { InMemoryBuildingRepository } from '../../../../infrastructure/repositories/building/in-memory/in-memory.building.repository';
import { Building } from '../../entities/building';
import { UpdateBuildingPort } from './update-building.port';
import { UpdateBuildingResult } from './update-building.result';
import { UpdateBuildingUseCase } from './update-building.use-case';
describe("UpdateBuildingUseCase", () => {
  let promise: Promise<UpdateBuildingResult>;

  let buildingRepository: InMemoryBuildingRepository;

  const building = generateBuilding();

  describe("when building is found", () => {
    beforeEach(() => {
      setup({
        id: building.id,
        name: "New name",
        type: "BuildingBase",
      }, [building]);
    });
    it("should return the updated building", async () => {
      expect(promise).resolves.toEqual({
        ...building,
        name: "New name",
        type: "BuildingBase",
        version: 2,
      });
    })
    it("should update the building", async () => {
      await promise;
      expect(buildingRepository.buildings).toEqual([{
        ...building,
        name: "New name",
        type: "BuildingBase",
        version: 2,
      }])
    });
  });

  describe("when building is not found", () => {
    beforeEach(() => {
      setup({
        id: building.id,
        name: "New name",
        type: "BuildingBase",
      });
    });
    it("should throw an error", async () => {
      expect(promise).rejects.toThrow(NotFoundError);
    });
  });
  function setup(port: UpdateBuildingPort, buildings: Building[] = []): void {
    buildingRepository = new InMemoryBuildingRepository(buildings)
    const useCase = new UpdateBuildingUseCase(buildingRepository);

    promise = useCase.execute(port);
  }
});

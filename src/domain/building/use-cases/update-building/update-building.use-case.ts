import { Inject } from '@nestjs/common';
import { NotFoundError } from '../../../../common/errors/not-found.error';
import { BUILDING_REPOSITORY } from '../../../../common/injectable.constant';
import { Building, updateBuilding } from '../../entities/building';
import { BuildingRepository } from '../../gateways/building.repository';
import { UpdateBuildingPort } from './update-building.port';
import { UpdateBuildingResult } from './update-building.result';
export class UpdateBuildingUseCase {
  constructor(
    @Inject(BUILDING_REPOSITORY)
    private readonly buildingRepository: BuildingRepository
  ) {}

  async execute(port: UpdateBuildingPort): Promise<UpdateBuildingResult> {
    const building = await this.buildingRepository.findOneById(port.id);

    if (!building) {
      throw new NotFoundError({ id: port.id });
    }

    const updatedBuilding: Building = updateBuilding(
      building,
      {
        ...port, 
        address: {
          ...port.address ?? building.address,
          completeAddress: "",
        },
      },
    )

    await this.buildingRepository.update(updatedBuilding);

    return updatedBuilding;
  }
}
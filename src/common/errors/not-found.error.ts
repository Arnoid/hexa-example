export class NotFoundError extends Error {
  constructor(public readonly meta: {
    id: string
  }) {
    super("Entity not found");
  }
}
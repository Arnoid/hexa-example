import { Body, Controller, Delete, Get, Param, Patch, Post, Put } from "@nestjs/common";
import { z } from "zod";
import { Building } from "../../../domain/building/entities/building";
import { GetBuildingUseCase } from "../../../domain/building/use-cases/get-building/get-building.use-case";
import { UpdateBuildingUseCase } from "../../../domain/building/use-cases/update-building/update-building.use-case";
import { buildingTypeSchema } from "../../../domain/building/value-objects/validators/building-type.validator";
import { idSchema } from "../../../domain/building/value-objects/validators/id.validator";
import { missionKindSchema } from "../../../domain/building/value-objects/validators/mission-kind.validator";
import { CreateBuildingUseCase } from './../../../domain/building/use-cases/create-building/create-building.use-case';

export type CreateBuildingRequestBody = Pick<Building, "name" | "type" | "agencyId">;

export type CreateBuildingResponseBody = Building;

export type UpdateBuildingRequestBody = Pick<Building, "name" | "type">;

export type UpdateBuildingResponseBody = Building;

export type PatchBuildingRequestBody = Partial<Pick<Building, "name" | "type">>;

export type PatchBuildingResponseBody = Building;

export type GetBuildingResponseBody = Building;

export type DeleteBuildingResponseBody = {};

@Controller("building")
export class BuildingController {
  constructor (
    private readonly createBuildingUseCase: CreateBuildingUseCase,
    private readonly updateBuildingUseCase: UpdateBuildingUseCase,
    private readonly getBuildingUseCase: GetBuildingUseCase,
  ) {}

  @Post("/")
  async create(
    @Body() body: CreateBuildingRequestBody,
  ): Promise<CreateBuildingResponseBody> {
    const sanitizedInput = z
      .object({
        name: z.string().trim().min(1),
        type: buildingTypeSchema,
        agencyId: idSchema,
        address: z.object({
          address1: z.string().trim().min(0).nullable().optional().default(null),
          zipCode: z.string().trim().min(0).nullable().optional().default(null),
          city: z.string().trim().min(0).nullable().optional().default(null),
        }),
        origin: z.object({
          target: idSchema,
          kind: missionKindSchema,
        })
      })
      .parse(body);
    return this.createBuildingUseCase.execute(sanitizedInput);
  }

  @Put("/:id")
  async update(
    @Param() id: string,
    @Body() body: UpdateBuildingRequestBody,
  ): Promise<UpdateBuildingResponseBody> {
    const sanitizedInput = z
      .object({
        id: idSchema,
        name: z.string().trim().min(1),
        type: buildingTypeSchema,
        address: z.object({
          address1: z.string().trim().min(0).nullable().optional().default(null),
          zipCode: z.string().trim().min(0).nullable().optional().default(null),
          city: z.string().trim().min(0).nullable().optional().default(null),
        }),
      })
      .parse({
        ...body,
        id,
      });
    return this.updateBuildingUseCase.execute(sanitizedInput);
  }

  @Patch("/:id")
  async patch(
    @Param() id: string,
    @Body() body: PatchBuildingRequestBody,
  ): Promise<PatchBuildingResponseBody> {
    const sanitizedInput = z
      .object({
        id: idSchema,
        name: z.string().trim().min(1).optional(),
        type: buildingTypeSchema.optional(),
      })
      .parse({
        ...body,
        id,
      });
    return this.updateBuildingUseCase.execute(sanitizedInput);
  }

  @Get("/:id")
  async getById(
    @Param() id: string,
  ): Promise<GetBuildingResponseBody> {
    const sanitizedInput = z.object({ id: idSchema }).parse({ id });
    return this.getBuildingUseCase.execute(sanitizedInput);
  }

  @Delete("/:id")
  async delete(
    @Param() id: string,
  ): Promise<DeleteBuildingResponseBody> {
    const sanitizedInput = z.object({ id: idSchema }).parse({ id });
    return this.getBuildingUseCase.execute(sanitizedInput);
  }
}

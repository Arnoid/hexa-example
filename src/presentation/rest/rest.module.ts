import { Module } from "@nestjs/common";
import { BuildingController } from './controllers/building.controller';

import { DomainModule } from "../../domain/domain.module";

@Module({
  controllers: [
    BuildingController
  ],
  imports: [DomainModule],
})
export class RestModule {}

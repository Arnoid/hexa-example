import { ClassProvider } from "@nestjs/common";

import { MESSAGE_BROKER } from "../../common/injectable.constant";
import { MessageBroker } from "../../domain/building/gateways/message-broker";
import { RabbitMqMessageBroker } from "./rabbitmq.message-broker";

export const MessageBrokerProvider: ClassProvider<MessageBroker> = {
  provide: MESSAGE_BROKER,
  useClass: RabbitMqMessageBroker,
};

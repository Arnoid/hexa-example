import { MessageBroker } from "../../domain/building/gateways/message-broker";

export type InMemoryMessage = {
  key: string,
  message: any,
};

export class InMemoryMessageBroker implements MessageBroker {
  constructor(public readonly messages: InMemoryMessage[] = []) {}
  send(key: string, message: any): Promise<void> {
    this.messages.push({
      key,
      message,
    });
    return Promise.resolve();
  }
}
import { BuildingCounterRepository } from "../../../../domain/building/gateways/building-counter.repository";

export class InMemoryBuildingCounterRepository implements BuildingCounterRepository {
  private index: number;

  constructor(private readonly numbers: string[] = []) {
    this.index = 0;
  }
  getNextNumber(): Promise<string> {
    const result = this.numbers[this.index];
    this.index++;
    return Promise.resolve(result);
  }
}
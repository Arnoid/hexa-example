import { model, Schema } from 'mongoose';
import { BuildingCounter } from '../../../../domain/building/entities/building-counter';

const types = Schema.Types;

const buildingCounterSchema = new Schema(
  {
    key: { type: types.String, required: true, unique: true },
    value: { type: types.Number },
  },
  {
    timestamps: true,
  },
);

export const BuildingCounterModel = model<BuildingCounter>('BuildingCounter', buildingCounterSchema, "counters");

import { BuildingCounterRepository } from './../../../../domain/building/gateways/building-counter.repository';
import { BuildingCounterModel } from './building-counter.model';
export class MongodbBuildingCounterRepository implements BuildingCounterRepository {
  async getNextNumber(): Promise<string> {
    const { value } = await BuildingCounterModel.findOneAndUpdate(
      {
        key: "BUILDING",
      },
      { $inc: { value: 1 } },
      { upsert: true, new: true },
    ).lean();

    return `5${value.toString().padStart(8, "0")}`;
  }
}
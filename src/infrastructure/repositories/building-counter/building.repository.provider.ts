import { ClassProvider } from "@nestjs/common";
import { BUILDING_COUNTER_REPOSITORY } from '../../../common/injectable.constant';
import { BuildingCounterRepository } from '../../../domain/building/gateways/building-counter.repository';
import { MongodbBuildingCounterRepository } from './mongodb/mongodb.building-counter.repository';

export const BuildingCounterRepositoryProvider: ClassProvider<BuildingCounterRepository> = {
  provide: BUILDING_COUNTER_REPOSITORY,
  useClass: MongodbBuildingCounterRepository,
};

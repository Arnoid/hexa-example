import { ClassProvider } from "@nestjs/common";
import { MongodbBuildingRepository } from './mongodb/mongodb.building.repository';

import { BUILDING_REPOSITORY } from "../../../common/injectable.constant";
import { BuildingRepository } from "../../../domain/building/gateways/building.repository";

export const BuildingRepositoryProvider: ClassProvider<BuildingRepository> = {
  provide: BUILDING_REPOSITORY,
  useClass: MongodbBuildingRepository,
};

import { Building } from '../../../../domain/building/entities/building';
import { BuildingRepository } from '../../../../domain/building/gateways/building.repository';
export class InMemoryBuildingRepository implements BuildingRepository {
  constructor(public readonly buildings: Building[] = []) {}
  create(building: Building): Promise<void> {
    this.buildings.push(building);

    return Promise.resolve();
  }

  findOneById(id: string): Promise<Building | null> {
    return Promise.resolve(this.buildings.find((building) => building.id === id) ?? null);
  }

  update(building: Building): Promise<void> {
    const buildingIndex = this.buildings.findIndex((b) => b.id === building.id);

    if (buildingIndex === -1) {
      return Promise.resolve();
    }

    this.buildings[buildingIndex] = building;

    return Promise.resolve();
  }

  deleteById(id: string): Promise<void> {
    const buildingIndex = this.buildings.findIndex((building) => building.id === id);

    if (buildingIndex === -1) {
      return Promise.resolve();
    }
    
    this.buildings.splice(buildingIndex, 1);

    return Promise.resolve();
  }
}
import { model, Schema } from 'mongoose';
import { Building } from '../../../../domain/building/entities/building';

const buildingSchema = new Schema<Building>({
  name: { type: String, required: true },
  number: { type: String, required: true },
  type: { type: String, required: true },
});

export const BuildingModel = model<Building>('Building', buildingSchema, "buildings");

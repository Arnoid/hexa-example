import { Types } from "mongoose";
import { Building } from '../../../../domain/building/entities/building';
import { BuildingRepository } from '../../../../domain/building/gateways/building.repository';
import { BuildingModel } from './building.model';
export class MongodbBuildingRepository implements BuildingRepository{
  async create(building: Building): Promise<void> {
    await BuildingModel.create({
      _id: new Types.ObjectId(building.id),
      name: building.name,
      number: building.number,
      type: building.type,
    });

    return Promise.resolve();
  }

  async findOneById(id: string): Promise<Building | null> {
    return await BuildingModel.findById(id);
  }

  async update(building: Building): Promise<void> {
    await BuildingModel.updateOne({ _id: new Types.ObjectId(building.id )}, { $replaceWith: building })

    return Promise.resolve();
  }

  async deleteById(id: string): Promise<void> {
    await BuildingModel.deleteOne({ _id: new Types.ObjectId(id) });

    return Promise.resolve();
  }
}
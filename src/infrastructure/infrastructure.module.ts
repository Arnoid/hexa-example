import { Module } from "@nestjs/common";
import { MessageBrokerProvider } from "./message-broker/message-broker.provider";
import { BuildingCounterRepositoryProvider } from "./repositories/building-counter/building.repository.provider";

import { BuildingRepositoryProvider } from "./repositories/building/building.repository.provider";

const providers = [
  BuildingRepositoryProvider,
  BuildingCounterRepositoryProvider,
  MessageBrokerProvider,
];

@Module({
  imports: [],
  providers,
  exports: providers,
})
export class InfrastructureModule {}

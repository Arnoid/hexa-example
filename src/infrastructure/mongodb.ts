import { connect } from 'mongoose';

export class Mongodb {
  public async connect() {
    await connect('mongodb://127.0.0.1:27017/test');
  }
}
